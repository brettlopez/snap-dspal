#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "example_interface.h"
#include "rpcmem.h"

int local_calculator_sum(const int* vec, int vecLen, int64* res)
{
  int ii = 0;
  *res = 0;

  for (ii = 0; ii < vecLen; ++ii) {
    *res = *res + vec[ii];
  }
  return 0;
}
/*
int local_calculator_diff(const int* vec, int vecLen, int64* res)
{
  int ii = 0;
  *res = vec[0];
  for (ii = 1; ii < vecLen; ++ii) {
    *res = *res - vec[ii];
  }
  return 0;
}
*/

int calculator_test(int runLocal, int num)
{
  int nErr = 0;
  int* test = 0;
  int len = 0;
  int ii;
  int64 result = 0;
  double delta_us = 0;

  struct timespec stop, start;

  rpcmem_init();

  len = sizeof(*test) * num;
  printf("- allocate %d bytes from ION heap\n", len);
  if (0 == (test = (int*)rpcmem_alloc(0, RPCMEM_HEAP_DEFAULT, len))) {
    printf("Error: alloc failed\n");
    nErr = 1;
    goto bail;
  }

  printf("- creating sequence of numbers from 0 to %d\n", num - 1);
  for (ii = 0; ii < num; ++ii) {
    test[ii] = ii;
  }

  if (runLocal) {
    printf("- compute sum locally\n");
  	clock_gettime(CLOCK_MONOTONIC, &start);
    for (ii=0; ii < 1000; ++ii){
  	    local_calculator_sum(test, num, &result);
  	}
  	clock_gettime(CLOCK_MONOTONIC, &stop);
  	delta_us = (double)(stop.tv_sec - start.tv_sec) * 1000000 + (double)(stop.tv_nsec - start.tv_nsec) / 1000;
    printf("sum took %f\n", delta_us/1000);
    
    if (0 != local_calculator_sum(test, num, &result)) {
      printf("Error: local compute failed\n");
      nErr = 1;
      goto bail;
    }
  } else {
    printf("- compute sum on the aDSP\n");
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (ii=0; ii < 1000; ++ii){
	    example_interface_sum(test, num, &result);
	}
  	clock_gettime(CLOCK_MONOTONIC, &stop);
  	delta_us = (double)(stop.tv_sec - start.tv_sec) * 1000000 + (double)(stop.tv_nsec - start.tv_nsec) / 1000;
    printf("sum took %f\n", delta_us/1000);
    
    if (0 !=  example_interface_sum(test, num, &result)) {
      printf("Error: compute on aDSP failed\n");
      nErr = 1;
      goto bail;
    }
  }
  printf("- sum = %lld\n", result);

  clock_gettime(CLOCK_MONOTONIC, &start);
  for (ii=0; ii < 1000; ++ii){
  		example_interface_mult(2);
	}
  clock_gettime(CLOCK_MONOTONIC, &stop);
  delta_us = (double)(stop.tv_sec - start.tv_sec) * 1000000 + (double)(stop.tv_nsec - start.tv_nsec) / 1000;
  printf("mult took %f\n", delta_us/1000);


  // printf("- mult = %lld\n", result);
  /*

  if (runLocal) {
    printf("- compute sum locally\n");
    if (0 != local_calculator_diff(test, num, &result)) {
      printf("Error: local compute failed\n");
      nErr = 1;
      goto bail;
    }
  } else {
    printf("- compute sum on the aDSP\n");
    if (0 != calculator_diff(test, num, &result)) {
      printf("Error: compute on aDSP failed\n");
      nErr = 1;
      goto bail;
    }
  }
  printf("- diff = %lld\n", result);
  */
bail:
  if (test) {
    rpcmem_free(test);
  }
  rpcmem_deinit();
  return nErr;
}

int main(int argc, char* argv[])
{
  int nErr = 0;
  int runLocal = 0;
  // int logcat = 0;
  int num = 0;

  if (argc < 4) {
    nErr = 1;
    goto bail;
  }
  runLocal = atoi(argv[1]);
  // logcat = atoi(argv[2]);
  num = atoi(argv[3]);

  printf("\n- starting calculator test\n");

  nErr = calculator_test(runLocal, num);

bail:
  if (nErr) {
    printf("\nusage: %s <1/0 run locally> <1/0 logcat> <uint32 size>\n\n", argv[0]);
  } else {
    printf("- success\n\n");
  }
}
