// @author  Brett Lopez
// @contact btlopez@mit.edu

typedef struct __attribute__ ((packed)) sMotorpacket
{ 	
	pthread_mutex_t ipc_mutex;
	pthread_cond_t  ipc_condvar;
	uint16_t seq;
	uint16_t f[8];
} tMotorpacket;
