// @author  Brett Lopez
// @contact btlopez@mit.edu

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <dspal_version.h>
#include <dspal_log.h>
#include <pthread.h>

#include "pwm_vars.h"


#define PWM_MINIMUM_PULSE_WIDTH 1050
#define PWM_MAXIMUM_PULSE_WIDTH 2000


int pwm_init(void)
{
	int ret = 1;
	struct dspal_version_info version;
	int major_version, minor_version;

	dspal_get_version_info_ext(&version);
	strtok(version.version_string, "-.");
	major_version = atoi(strtok(NULL, "-."));
	minor_version = atoi(strtok(NULL, "-."));

	if (major_version < 1 || (major_version == 1 && minor_version < 1))
	{
		LOG_INFO("Unable to generate PWM signaling, current version: %d.%d, requires version 1.1 or above",
				major_version, minor_version);
		return 0;
	}
	/*
	 * Open PWM device
	 */
	fd = -1;
	fd = open("/dev/pwm-1", 0);

	if (fd > 0) {
		/*
		 * Configure PWM device
		 */
		
		// Define the initial pulse width and number of the GPIO to use for this signal definition.
		pwm_gpio[0].gpio_id = 4;
		pwm_gpio[0].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[1].gpio_id = 5;
		pwm_gpio[1].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[2].gpio_id = 6;
		pwm_gpio[2].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[3].gpio_id = 7;
		pwm_gpio[3].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[4].gpio_id = 49;
		pwm_gpio[4].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[5].gpio_id = 50;
		pwm_gpio[5].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[6].gpio_id = 51;
		pwm_gpio[6].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
		pwm_gpio[7].gpio_id = 52;
		pwm_gpio[7].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;

		// Describe the overall signal and reference the above array.
		// NOTE: num_gpios needs be even
		signal_definition.num_gpios = 8;
		signal_definition.period_in_usecs = PWM_MAXIMUM_PULSE_WIDTH;
		signal_definition.pwm_signal = &pwm_gpio[0];

		// Send the signal definition to the DSP.
		if (ioctl(fd, PWM_IOCTL_SIGNAL_DEFINITION, &signal_definition) != 0) {
			ret = 0;
		}

		// Retrieve the shared buffer which will be used below to update the desired
		// pulse width.
		if (ioctl(fd, PWM_IOCTL_GET_UPDATE_BUFFER, &update_buffer) != 0)
		{
			ret = 0;
		}
		pwm = &update_buffer->pwm_signal[0];
		update_buffer->reserved_1 = 0;

		// Wait for the ESC's to ARM:
		usleep(1000000 * 1); // wait 1 seconds

		LOG_INFO("Initialized");

	} else {
		ret = 0;
	}

	return ret;
}

void pwm_update(const uint16_t* f, uint16_t fLen){
	// LOG_INFO("Updating...");
	pwm[0].pulse_width_in_usecs = f[0];
	pwm[1].pulse_width_in_usecs = f[1];
	pwm[2].pulse_width_in_usecs = f[2];
	pwm[3].pulse_width_in_usecs = f[3];
	pwm[4].pulse_width_in_usecs = f[4];
	pwm[5].pulse_width_in_usecs = f[5];
	pwm[6].pulse_width_in_usecs = f[6];
	pwm[7].pulse_width_in_usecs = f[7];
}

void pwm_close(void){
	/*
	 * Close the device ID
	 */
	close(fd);
	LOG_INFO("Closing port");
}
