// @author  Brett Lopez
// @contact btlopez@mit.edu

#pragma once

#include <dev_fs_lib_pwm.h>

#ifdef __cplusplus
extern "C" {
#endif

struct dspal_pwm pwm_gpio[DEV_FS_PWM_MAX_NUM_SIGNALS];
struct dspal_pwm_ioctl_signal_definition signal_definition;
struct dspal_pwm_ioctl_update_buffer *update_buffer;
struct dspal_pwm *pwm;
int fd;

#ifdef __cplusplus
}
#endif