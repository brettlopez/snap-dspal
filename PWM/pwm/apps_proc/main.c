// @author  Brett Lopez
// @contact btlopez@mit.edu

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <getopt.h>
#include <dspal_log.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#include "adspmsgd.h"
#include "rpcmem.h"
#include "pwm.h"
#include "struct.h"

volatile sig_atomic_t stop;

void inthand(int signum) {
    stop = 1;
}

int main(int argc, char *argv[])
{
    signal(SIGINT, inthand);

    LOG_INFO("Starting ESC interface");
    int status = 1;
    status = pwm_init();

    // Accocate shared memory for motor commands
    int segment_id;
    key_t shm_key = 6166520;
    const int shared_segment_size = sizeof(tMotorpacket);

    segment_id = shmget (shm_key, shared_segment_size, IPC_CREAT | S_IRUSR | S_IWUSR);
    tMotorpacket *pkt = (tMotorpacket*) shmat (segment_id, 0, 0);

    uint16_t* f;
    struct timespec timeToWait;
    struct timeval now;
    int rt, pred_seq, diff, timeout;
    pred_seq = 0;
    diff = 0;
    timeout = 0;

    while (!stop){
        if (!timeout){
            clock_gettime(CLOCK_REALTIME, &timeToWait);
            timeToWait.tv_sec = timeToWait.tv_sec+4.;

            pthread_mutex_lock(&pkt->ipc_mutex);
            rt = pthread_cond_timedwait(&pkt->ipc_condvar,&pkt->ipc_mutex,&timeToWait);
            if (rt!=0){
                LOG_ERR("Timeout - make sure snap stack is running and receiving a pose then restart esc_interface");
                timeout = 1;
                /* Detach the shared memory segment. */
                shmdt (pkt);
                /* Deallocate the shared memory segment.*/
                shmctl (segment_id, IPC_RMID, 0);

            }
            else{
                // Safely access shared memory
                if (pred_seq==0) pred_seq = pkt->seq;
                diff = pkt->seq-pred_seq;

                f = pkt->f;
                int fLen = sizeof(pkt->f);
                
                pwm_update(f,fLen);
                pthread_mutex_unlock(&pkt->ipc_mutex);
                if (diff>0) {
                    // Debug message for packet drop - happens rarely so removed
                    // if (diff>1) LOG_ERR("Warning: missed %d packets",diff);
                    pred_seq += diff;
                }
                // Only happens when seq number wraps
                else if (diff < 0) pred_seq += diff;
                pred_seq=pred_seq+1;
            }
        }
    }
    
    // Detach shared memory
    shmdt (pkt); 

    pwm_close();

    LOG_INFO("Terminating ESC interface");

	return 0;
}
